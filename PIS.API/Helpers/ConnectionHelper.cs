﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Npgsql;

namespace PIS.API.Helpers
{
    public class ConnectionHelper<TModel> where TModel : class
    {
        public string _connectionString { get; set; } = "Server=172.19.0.57;Port=5432;Database=pis_mig_test;User Id=pis; Password=p;SearchPath=public;";

        private NpgsqlConnection _con;

        public ConnectionHelper()
        {
            if (_con == null)
            {
                Connect();
            }
        }
        private void Connect()
        {
            _con = new NpgsqlConnection(_connectionString);
            if (_con.State != System.Data.ConnectionState.Open)
            {
                _con.Open();
            }
        }

        public List<TModel> Query(string sql, object parameters)
        {
            return _con.Query<TModel>(sql, parameters).ToList();
        }
    }
}
