﻿using Microsoft.AspNetCore.Mvc;
using PIS.API.Models;
using PIS.SOA.Base.User;
using Microsoft.AspNetCore.Authorization;

namespace PIS.API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        public ActionResult<User> GetUsers()
        {
            var userRepo = new UserRepo();
            return Ok(userRepo.GetUsers());
        }
    }
}
