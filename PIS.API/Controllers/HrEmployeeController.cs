﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PIS.SOA.Base.HrEmployee;
using System.Collections.Generic;

namespace PIS.SOA.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class HrEmployeeController : Controller
    {
        private readonly IHrEmployeeRepo _hrEmployeeRepo;
        public HrEmployeeController()
        {
            _hrEmployeeRepo = new HrEmployeeRep();
        }

        [HttpGet]
        public ActionResult<List<HrEmployeeModel>> Get()
        {
            return _hrEmployeeRepo.GetAll();
        }
    }
}
