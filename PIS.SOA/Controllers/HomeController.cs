﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PIS.SOA.Base.User;

namespace PIS.SOA.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View("Index");
        }

        public IActionResult Register()
        {
            return View("Register");
        }

        public IActionResult Users()
        {
            var users = new UserRepo().GetUsers();
            return View("Protected", users);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RegisterPost(UserCreateModel userCreateModel)
        {
            if (string.IsNullOrEmpty(userCreateModel.username))
            {
                return Content("Please provide the username...");
            }

            if (string.IsNullOrEmpty(userCreateModel.password))
            {
                return Content("Please provide the password...");
            }
            string encryptedPassword = PIS.SOA.Base.Helper.PasswordHasher.EncryptPassword(userCreateModel.password);
            userCreateModel.password = encryptedPassword;
            var result = new UserRepo().CreateUser(userCreateModel);
            if (result)
            {
                return Content("Successfully Register New User...");
            }
            return Content("Failed to Register User...");
        }
    }

}