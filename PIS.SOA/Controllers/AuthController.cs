﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using PIS.SOA.Base.User;
using PIS.SOA.Dtos;
using PIS.SOA.Base.RefreshToken;
using PIS.SOA.Helpers;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using PIS.SOA.Base.UserRole;
using System.Linq;


namespace PIS.SOA.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthRepo _authRepo;
        private readonly IRefreshTokenRepo _refreshTokenRepo;
        private readonly IUserRoleRepo _userRoleRepo;
        public AuthController(IAuthRepo authRepo, IRefreshTokenRepo refreshTokenRepo, IUserRoleRepo userRoleRepo)
        {
            _authRepo = authRepo;
            _refreshTokenRepo = refreshTokenRepo;
            _userRoleRepo = userRoleRepo;
        }
        [HttpPost("login")]
        public ActionResult<TokenResponse> Login([FromBody] UserForLoginDto userForLoginDto)
        {
            UserModel userFromRepo = _authRepo.Login(userForLoginDto.username.ToLower(), userForLoginDto.password, userForLoginDto.client_id);

            if (userFromRepo == null)
                return Unauthorized();


            var refreshTokenModel = new RefreshTokenModel
            {
                //id = Guid.NewGuid().ToString(),
                login_date_time = DateTime.Now,
                client_secret = userFromRepo.client_secret,
                refresh_token = TokenHelper.GenerateRefreshToken(),
                client_id = userForLoginDto.client_id,
            };


            if (!_refreshTokenRepo.AddToken(refreshTokenModel))
                return BadRequest(new
                {
                    error = "could not add token to the database"
                });

            var identityModel = new IdentityModel();
            identityModel.client_id = userForLoginDto.client_id;
            identityModel.username = userForLoginDto.username;
            identityModel.name_en = userFromRepo.name_en;

            //var userRolesFromDb = _userRoleRepo.GetRolesByUserId(userFromRepo.id);
            //var userRoleModel = userRolesFromDb.FirstOrDefault();
            //identityModel.role = userRoleModel.role_name;

            var identityManager = new IdentityManager(identityModel);


         RefreshTokenModel newRefreshTokenToReturn = _refreshTokenRepo.GetLatestToken(refreshTokenModel.client_secret, refreshTokenModel.client_id);

            var response = new TokenResponse()
            {
                access_token = new JwtSecurityTokenHandler()
                .WriteToken(TokenHelper.GenerateToken(identityManager.GetClaims())),
                message = "success",
                refresh_token = newRefreshTokenToReturn.refresh_token,
                expires_in = TokenHelper.TokenExpireIntervalInMinute,
                success = true
            };
            return Ok(response);
        }

        [HttpGet("refreshToken")]
        [Authorize]
        public ActionResult<TokenResponse> RefreshAccessToken([FromQuery] string oldRefreshToken, [FromQuery] int? client_id)
        {
            //Defensive programming concept: validation everything first
            if (string.IsNullOrEmpty(oldRefreshToken) || !client_id.HasValue)
                return Unauthorized();

            //TODO: 1. Get the refresh token from the database by checking with old refresh token
            var tokenFromDb = _refreshTokenRepo.GetRefreshToken(oldRefreshToken, client_id);

            //TODO: 2. If token is returned from database then delete/update the existing refresh token and generate new one. If not returned throw unauthorized access 401
            if (tokenFromDb == null)
                return BadRequest(new
                {
                    error = "Token Not found"
                });

            bool hasExpired = _refreshTokenRepo.ExpireToken(tokenFromDb);
            var refresh_token = TokenHelper.GenerateRefreshToken();
            //TODO: 3. Save the newly generated refresh token to the database
            bool hasNewRefreshTokenGenerated = _refreshTokenRepo.AddToken(new RefreshTokenModel
            {
                //id = Guid.NewGuid().ToString(),
                refresh_token = refresh_token,
                client_id = client_id
            });
            //TODO: 4. Generate new access token and send the newly generated access token and refresh token to the client
            if (hasExpired && hasNewRefreshTokenGenerated)
            {
                var access_token = new JwtSecurityTokenHandler().WriteToken(TokenHelper.GenerateToken(new List<Claim>()
                {
                    new Claim("users.edit","true")
                }));

                return new TokenResponse()
                {
                    access_token = access_token,
                    refresh_token = refresh_token,
                    expires_in = TokenHelper.TokenExpireIntervalInMinute,
                    message = "Token created successfully"
                };
            }
            else
            {
                return BadRequest(new
                {
                    error = "Failed to expire token or create a new token"
                });
            }
        }

        [HttpGet("Hello")]
        public string Hello()
        {
            return "hello".ToUpper();
        }

        [HttpGet("Register")]
        public IActionResult Register()
        {
            return View("Register");
        }

        [HttpPost("LoginWithToken")]
        public IActionResult Login([FromBody] LoginWithTokenDto loginWithTokenDto)
        {

            if (string.IsNullOrEmpty(loginWithTokenDto.token) && loginWithTokenDto.client_id == null)
                return Json(new { message = "please provide valid client_id and token", success = false }); 
            TokenResponse response = new TokenResponse();

            RefreshTokenModel model =  new UserRepo().CheckToken(loginWithTokenDto.token, loginWithTokenDto.client_id);

            response.refresh_token = model.refresh_token;
            response.message = "Refresh token successfully retreived";
            response.success = true;

            return Ok(response);
        }
    }

    public class LoginWithTokenDto
    {
        public string token { get; set; }
        public int? client_id { get; set; }
    }

}
