﻿namespace PIS.SOA.Dtos
{
    public class TokenResponse
    {
        public string message { get; set; }
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public int expires_in { get; set; }
        public bool success { get; set; }
    }
}
