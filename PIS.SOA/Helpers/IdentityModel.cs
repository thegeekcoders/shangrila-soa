﻿namespace PIS.SOA.Helpers
{
    public class IdentityModel
    {
        public int? client_id { get; set; }
        public string username { get; set; }
        //public string[] roles { get; set; } = new string[] { };
        public string role { get; set; }
        public string name_en { get; set; }
        public string[] permissions { get; set; } = new string[] { };
    }
}
