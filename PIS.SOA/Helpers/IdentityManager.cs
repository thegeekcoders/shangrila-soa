﻿using System.Collections.Generic;
using System.Security.Claims;

namespace PIS.SOA.Helpers
{
    public class IdentityManager
    {
        public IdentityModel _identityModel { get; set; }
        public IdentityManager(IdentityModel identityModel)
        {
            _identityModel = identityModel;
        }
        public List<Claim> GetClaims()
        {
            var claims = new List<Claim>();

            claims.Add(new Claim("client_id", _identityModel.client_id.ToString()));
            claims.Add(new Claim("username", _identityModel.username));
            //claims.Add(new Claim("role", _identityModel.role));
            claims.Add(new Claim("name_en", _identityModel.name_en));

            //foreach (string role in _identityModel.roles)
            //{

            //}
            return claims;
        }

    }
}
