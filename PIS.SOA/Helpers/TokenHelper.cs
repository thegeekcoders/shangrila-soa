﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace PIS.SOA.Helpers
{
    public static class TokenHelper
    {
        public static int TokenExpireIntervalInMinute { get; private set; }
        public static string ValidAudience { get; private set; }
        public static string ValidIssuer { get; private set; }
        public static string securityKey { get; private set; }
        public static SymmetricSecurityKey symmetricSecurity { get; private set; }
        public static SigningCredentials signingCredentials { get; private set; }

        static TokenHelper()
        {
            TokenExpireIntervalInMinute = 60;
            securityKey = "super secret key";
            symmetricSecurity = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
            signingCredentials = new SigningCredentials(symmetricSecurity, SecurityAlgorithms.HmacSha512Signature);
            ValidAudience = "demo";
            ValidIssuer = "app";
        }

        public static JwtSecurityToken GenerateToken(IList<Claim> claims)
        {
            JwtSecurityToken token = new JwtSecurityToken(
                claims: claims,
                issuer: ValidIssuer,
                audience: ValidAudience,
                expires: DateTime.Now.AddMinutes(TokenExpireIntervalInMinute),
                signingCredentials: signingCredentials
                );
            return token;
        }
        public static string GenerateRefreshToken()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }
    }
}
