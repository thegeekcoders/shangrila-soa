﻿using System.Threading.Tasks;
using Xunit;
using System.Net.Http;

namespace PIS.SOA.IntegrationTests
{
    public class UsersControllerShould : IntegrationTestInitializer<PIS.API.Startup>
    {
        public UsersControllerShould()
        {

        }

        [Fact]
        public void Return_401_On_UnAuthrized_On_GetUsers()
        {

        }

        private async Task<string> GetAccessToken(string username, string password)
        {
            var user = new UserModel()
            {
                client_id = 1,
                username = username,
                password = password
            };
            var response = await HttpClient.PostAsJsonAsync("api/auth/login", user);

            if (!response.IsSuccessStatusCode) return null;

            var tokenResponse = await response.Content.ReadAsAsync<TokenResponse>();
            return tokenResponse?.access_token;
        }
    }

    public class UserModel
    {
        public int? client_id { get; set; }
        public string client_secret { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class TokenResponse
    {
        public string message { get; set; }
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public int expires_in { get; set; }
    }
}
