﻿using System.Threading.Tasks;
using Xunit;

namespace PIS.SOA.IntegrationTests
{
    public class ValuesControllerShould : IntegrationTestInitializer<PIS.API.Startup>
    {
        [Fact]
        public async Task Return_Values_OnGetRequest()
        {
            var response = await HttpClient.GetAsync("/api/values");
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            Assert.Contains("value1", responseString);
        }
    }
}
