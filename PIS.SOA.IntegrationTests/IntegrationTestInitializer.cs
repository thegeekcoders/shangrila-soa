﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;

namespace PIS.SOA.IntegrationTests
{
    public class IntegrationTestInitializer<TStartup> where TStartup: class
    {
        public TestServer testServer { get; private set; }
        public HttpClient HttpClient { get; private set; }
        public IWebHostBuilder webHostBuilder { get; private set; }

        public IntegrationTestInitializer()
        {
            webHostBuilder = new WebHostBuilder()
                 .UseStartup<TStartup>()
                 .UseEnvironment("Development");

            testServer = new TestServer(webHostBuilder);
            HttpClient = testServer.CreateClient();
        }

    }
}
