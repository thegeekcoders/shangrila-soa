using System;
using Xunit;

namespace PIS.SOA.IntegrationTests
{
    public class AuthControllerShould : IntegrationTestInitializer<PIS.SOA.Startup>
    {
        [Fact]
        public void Return_Acess_Token_On_Login()
        {
            string guid = Guid.NewGuid().ToString();
            Assert.NotNull(guid);
        }

        [Fact]
        public void Return_New_Refreshed_Acess_Token_On_Token_Expired()
        {
        }
    }
}
