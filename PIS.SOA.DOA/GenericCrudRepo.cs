﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PIS.SOA.DbServiceUtility
{
    public class GenericCrudRepo<TModel> : IGenericRepo<TModel> 
        where TModel : class, new()
    {
        public bool Complete()
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<TModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public TModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(TModel model)
        {
            throw new NotImplementedException();
        }

        public bool Update(TModel model)
        {
            throw new NotImplementedException();
        }
    }
}
