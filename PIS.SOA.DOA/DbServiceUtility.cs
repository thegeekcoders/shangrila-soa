﻿using System.Collections.Generic;
using Npgsql;
using Dapper;
using System.Linq;

namespace PIS.SOA.DbServiceUtility
{
    public class DbServiceUtility<TModel> where TModel : class
    {
        private string _connectionString { get; set; } = "Server=110.44.116.135;Port=57432;Database=pis_oauth_db;User Id=pis; Password=p;SearchPath=public;";

        private NpgsqlConnection _con;

        public DbServiceUtility()
        {
            if (_con == null)
            {
                Connect();
            }
        }
        private void Connect()
        {
            _con = new NpgsqlConnection(_connectionString);
            if (_con.State != System.Data.ConnectionState.Open)
            {
                _con.Open();
            }
        }

        public List<TModel> Query(string sql, object parameters)
        {
            return _con.Query<TModel>(sql, parameters).ToList();
        }

        public bool Insert(string sql, object parameters)
        {
            return _con.Execute(sql, parameters) > 0;
        }

        public bool Update(string sql, object parameters)
        {
            return _con.Execute(sql, parameters) > 0;
        }

        public bool Delete(string sql, object parameters)
        {
            return _con.Execute(sql, parameters) > 0;
        }

        public void Close()
        {
            _con.Close();
        }
    }
}
