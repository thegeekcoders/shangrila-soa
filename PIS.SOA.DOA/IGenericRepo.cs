﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PIS.SOA.DbServiceUtility
{
    public interface IGenericRepo<TModel> where TModel: class, new()
    {
        TModel GetById(int id);
        List<TModel> GetAll();
        bool Insert(TModel model);
        bool Update(TModel model);
        bool Delete(int id);
        bool Complete();
    }
}
