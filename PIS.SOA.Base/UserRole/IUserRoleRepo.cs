﻿using System.Collections.Generic;

namespace PIS.SOA.Base.UserRole
{
    public interface IUserRoleRepo
    {
        List<UserRoleModel> GetRolesByUserId(int user_id);
    }
}
