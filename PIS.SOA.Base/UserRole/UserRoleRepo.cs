﻿using System.Collections.Generic;
using PIS.SOA.DbServiceUtility;

namespace PIS.SOA.Base.UserRole
{
    public class UserRoleRepo : IUserRoleRepo
    {
        public List<UserRoleModel> GetRolesByUserId(int user_id)
        {
            var dbServiceUtility = new DbServiceUtility<UserRoleModel>();
            var rolesByUserFromDb = dbServiceUtility.Query(@"
            SELECT rrm.name_en as role_name from rbac_user_roles rur 
            INNER JOIN rbac_role_master rrm on rrm.id = rur.assigned_role_id
            WHERE rur.client_id = @client_id and rur.user_id = @user_id and rur.is_deleted=false
             ", new
            {
                user_id,
                client_id = 1
            });

            dbServiceUtility.Close();
            return rolesByUserFromDb;
        }
    }
}
