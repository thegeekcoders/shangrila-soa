﻿using System.Collections.Generic;

namespace PIS.SOA.Base.RefreshToken
{
    public interface IRefreshTokenRepo
    {
        List<RefreshTokenModel> GetRefreshTokens();
        RefreshTokenModel GetRefreshToken(string refresh_token, int? client_id);
        bool AddToken(RefreshTokenModel refreshToken);
        bool ExpireToken(RefreshTokenModel refreshToken);
        RefreshTokenModel GetLatestToken(string client_secret, int? client_id);

    }
}
