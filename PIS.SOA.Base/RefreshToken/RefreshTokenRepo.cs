﻿using System.Collections.Generic;
using PIS.SOA.DbServiceUtility;
using System.Linq;

namespace PIS.SOA.Base.RefreshToken
{
    public class RefreshTokenRepo : IRefreshTokenRepo
    {
        private readonly DbServiceUtility<RefreshTokenModel> _dbServiceUtility;

        public RefreshTokenRepo()
        {
            _dbServiceUtility = new DbServiceUtility<RefreshTokenModel>();
        }
        public bool AddToken(RefreshTokenModel refreshToken)
        {
            var status = _dbServiceUtility.Insert("insert into refresh_tokens(client_id,refresh_token,client_secret,login_date_time) values(@client_id,@refresh_token,@client_secret,@login_date_time)", refreshToken);
            _dbServiceUtility.Close();
            return status;
        }

        public bool ExpireToken(RefreshTokenModel refreshToken)
        {
            //var status = _dbServiceUtility.Update(@"update refresh_tokens set refresh_token = @refresh_token and client_id = @client_id where 
            //    refresh_token = @refresh_token and client_id = @client_id
            //    ", refreshToken);
            var status = _dbServiceUtility.Delete(@"delete from refresh_tokens where refresh_token = @refresh_token and client_id = @client_id", refreshToken);
            _dbServiceUtility.Close();
            return status;
        }

        public RefreshTokenModel GetLatestToken(string client_secret, int? client_id)
        {
            var refreshToken = _dbServiceUtility.Query(@"SELECT * from refresh_tokens where client_secret =  @client_secret and client_id = @client_id
                        order by login_date_time desc", new
            {
                client_secret,
                client_id
            }).FirstOrDefault();
            _dbServiceUtility.Close();

            return refreshToken;
        }

        public RefreshTokenModel GetRefreshToken(string refresh_token, int? client_id)
        {
            var refreshToken = _dbServiceUtility.Query(@"select * from refresh_tokens where refresh_token = @refresh_token and client_id = @client_id", new
            {
                refresh_token,
                client_id
            }).FirstOrDefault();
            _dbServiceUtility.Close();
            return refreshToken;
        }

        public List<RefreshTokenModel> GetRefreshTokens()
        {
            var refreshTokens = _dbServiceUtility.Query(@"select * from refresh_tokens", null);
            _dbServiceUtility.Close();
            return refreshTokens;
        }
    }
}
