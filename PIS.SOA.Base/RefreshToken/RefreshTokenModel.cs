﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace PIS.SOA.Base.RefreshToken
{
    [Table("refresh_tokens")]
   public class RefreshTokenModel
    {
        [Key]
        public string id { get; set; }
        public int? client_id { get; set; }
        public string refresh_token { get; set; }
        public string client_secret { get; set; }
        public DateTime? login_date_time { get; set; }

    }
}
