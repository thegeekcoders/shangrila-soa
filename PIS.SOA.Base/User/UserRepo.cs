﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PIS.SOA.Base.User
{
    public class UserRepo
    {
        public List<UserModel> GetUsers()
        {
            var helper = new DbServiceUtility.DbServiceUtility<UserModel>();
            var userToReturn = helper.Query("select * from rbac_user order by last_loggedin desc", null);
            return userToReturn;
        }

        public bool CreateUser(UserCreateModel userCreateModel)
        {
            var helper = new DbServiceUtility.DbServiceUtility<UserCreateModel>();
            bool result = helper.Insert("insert into rbac_user(username,password,name_en,name_np,pis_number) values(@username,@password,@name_en,@name_np,@pis_number)", userCreateModel);
            return result;
        }

        public RefreshToken.RefreshTokenModel CheckToken(string token, int? client_id)
        {
            var sql = $@"
                   SELECT * from refresh_tokens where refresh_token =  @token and client_id = @client_id
                ";

            var result = new DbServiceUtility.DbServiceUtility<RefreshToken.RefreshTokenModel>().Query(sql, new
            {
                token,
                client_id
            });

            if(result != null)
            {
                return result.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }
    }


    public class UserCreateModel
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
        //public string email { get; set; }
        public string name_en { get; set; }
        [Required]
        public string name_np { get; set; }
        [Required]
        public string pis_number { get; set; }
    }

}
