﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PIS.SOA.Base.User
{
  public  interface IAuthRepo
    {
        UserModel Login(string username, string password, int? client_id);
        bool UserExists(string username, string password);
    }
}
