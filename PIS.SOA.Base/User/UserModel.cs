﻿using System;

namespace PIS.SOA.Base.User
{
    public class UserModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string name_en { get; set; }
        public string name_np { get; set; }
        public bool status { get; set; }
        public DateTime? last_loggedin { get; set; }
        public string client_secret { get; set; }
    }
}
