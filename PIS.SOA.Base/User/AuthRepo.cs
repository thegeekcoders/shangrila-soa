﻿using System.Threading.Tasks;
using PIS.SOA.DbServiceUtility;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System;

namespace PIS.SOA.Base.User
{
    public class AuthRepo : IAuthRepo
    {
        public UserModel Login(string username, string password, int? client_id)
        {
            Random random = new Random();
            string client_secret = Guid.NewGuid().ToString() + random.Next(0, 1000);
            var utility = new DbServiceUtility<UserModel>();
            UserModel userModel = utility.Query("select * from rbac_user where username = @username and client_id = @client_id", new
            {
               username,
               client_id
            }).FirstOrDefault();

            if (userModel == null)
            {
                return null;
            }

            if (!VerifyPasswordHash(userModel.password,password))
                return null;

            userModel.last_loggedin = DateTime.Now;
            userModel.password = Helper.PasswordHasher.EncryptPassword(password);

            if(userModel.client_secret == null)
            {
                userModel.client_secret = client_secret;
                utility.Update("update rbac_user set last_loggedin = @last_loggedin, client_secret=@client_secret WHERE username = @username AND password = @password", userModel);
            }
            else
            {
                utility.Update("update rbac_user set last_loggedin = @last_loggedin WHERE username = @username AND password = @password", userModel);
            }
          

            return userModel;
        }

        public bool UserExists(string username, string password)
        {
            var utility = new DbServiceUtility<UserModel>();
            UserModel userModel = utility.Query("select * from rbac_user where username = @username and password = @password", new
            {
                username,
                password
            }).FirstOrDefault();

            if (userModel != null)
            {
                return true;
            }
            return false;
        }

        private bool VerifyPasswordHash(string pwFromDB, string password)
        {
            if(pwFromDB == Helper.PasswordHasher.EncryptPassword(password))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
