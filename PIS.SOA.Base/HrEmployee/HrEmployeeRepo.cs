﻿using System.Collections.Generic;
using PIS.SOA.DbServiceUtility;

namespace PIS.SOA.Base.HrEmployee
{
    public class HrEmployeeRep : IHrEmployeeRepo

    {
        private readonly DbServiceUtility<HrEmployeeModel> _dbServiceUtility;

        public HrEmployeeRep()
        {
            _dbServiceUtility = new DbServiceUtility<HrEmployeeModel>();
        }
        public List<HrEmployeeModel> GetAll()
        {
            return _dbServiceUtility.Query(@"select * from vw_hr_employee limit 100", null);
        }
    }
}
