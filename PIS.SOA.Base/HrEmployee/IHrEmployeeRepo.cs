﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PIS.SOA.Base.HrEmployee
{
    public interface IHrEmployeeRepo
    {
        List<HrEmployeeModel> GetAll();
    }
}
