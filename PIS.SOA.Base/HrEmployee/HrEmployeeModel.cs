﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PIS.SOA.Base.HrEmployee
{
    public class HrEmployeeModel
    {
        public int id { get; set; }
        public string full_name_en { get; set; }
        public string full_name_np { get; set; }
        public string position_name { get; set; }
        public string  current_office_name { get; set; }
        public string gender { get; set; }
        public DateTime dob_ad { get; set; }
        public string dob_bs { get; set; }
        public DateTime joined_date_ad { get; set; }
        public string joined_date_bs { get; set; }
        public string photo_path { get; set; }
    }
}
